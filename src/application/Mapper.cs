using org.clebi.cryptosock.domain;
using org.clebi.cryptosock.exposition;

namespace org.clebi.cryptosock.application
{
    internal class TradeMapper
    {
        public static Trade ToDomain(BinanceTradeEventDTO trade)
        {
            return new Trade(
                trade.Id,
                new Pair(trade.Pair),
                new Amount(trade.Price, trade.Quantity),
                new Transaction(trade.Buyer, trade.Seller, trade.OperationTime, trade.MarketMaker));
        }
    }
}

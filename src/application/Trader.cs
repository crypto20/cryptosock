using org.clebi.cryptosock.domain;
using org.clebi.cryptosock.exposition;

namespace org.clebi.cryptosock.application
{
    internal class Trader
    {
        private readonly ITradeRepository tradeRepository;

        public Trader(ITradeRepository tradeRepository) {
            this.tradeRepository = tradeRepository;
        }

        public void SaveTrade(BinanceTradeEventDTO trade)
        {
            tradeRepository.Save(TradeMapper.ToDomain(trade));
        }
    }
}

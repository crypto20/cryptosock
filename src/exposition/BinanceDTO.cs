using System.Text.Json.Serialization;

namespace org.clebi.cryptosock.exposition
{
    public record BinanceTradeEventDTO(
        [property: JsonPropertyName("e")] string Type,
        [property: JsonPropertyName("E")] long Time,
        [property: JsonPropertyName("s")] string Pair,
        [property: JsonPropertyName("t")] long Id,
        [property: JsonPropertyName("p")] double Price,
        [property: JsonPropertyName("q")] double Quantity,
        [property: JsonPropertyName("b")] long Buyer,
        [property: JsonPropertyName("a")] long Seller,
        [property: JsonPropertyName("T")] long OperationTime,
        [property: JsonPropertyName("m")] bool MarketMaker,
        [property: JsonPropertyName("M")] bool Ignore
    )
    { }
}

using System.Text.Json;
using System.Text.Json.Serialization;
using org.clebi.cryptosock.application;
using Websocket.Client;

namespace org.clebi.cryptosock.exposition
{
    public interface IWsConsumer {
        void Consume();
    }

    internal class BinanceWSConsumer: IWsConsumer
    {
        private readonly IWebsocketClient client;
        private readonly Trader trader;

        private readonly JsonSerializerOptions options = new()
        {
            NumberHandling = JsonNumberHandling.AllowReadingFromString | JsonNumberHandling.WriteAsString,
            PropertyNameCaseInsensitive = false
        };

        public BinanceWSConsumer(IWebsocketClient client, Trader trader)
        {
            this.client = client;
            this.trader = trader;
        }

        public void Consume()
        {
            client.ReconnectTimeout = TimeSpan.FromSeconds(30);
            client.ReconnectionHappened.Subscribe(info =>
                Console.WriteLine($"Reconnection happened, type: {info.Type}"));
            client.MessageReceived.Subscribe(msg =>
            {
                var trade = JsonSerializer.Deserialize<BinanceTradeEventDTO>(msg.Text, options);
                Console.WriteLine($"Event: {trade}");
                if (trade != null)
                {
                    trader.SaveTrade(trade);
                }
            });
            client.Start();
        }
    }
}

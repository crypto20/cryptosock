using Confluent.Kafka;
using org.clebi.cryptosock.domain;

namespace org.clebi.cryptosock.infrastructure
{
    internal class KafkaTradeRepository : ITradeRepository
    {
        private readonly IProducer<long, KafkaTradeDTO> producer;

        public KafkaTradeRepository(IProducer<long, KafkaTradeDTO> producer)
        {
            this.producer = producer;
        }

        public async void Save(Trade trade)
        {
            await producer.ProduceAsync(String.Format("trade-{0}-topic", trade.Pair.Symbol.ToLower()), new Message<long, KafkaTradeDTO>
            {
                Key = trade.Id,
                Value = new KafkaTradeDTO(trade.Id, trade.Transaction.Time, trade.Pair.Symbol, trade.Amount.Price, trade.Amount.Quantity, trade.Amount.Total)
            });
        }
    }
}

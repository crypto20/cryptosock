namespace org.clebi.cryptosock.infrastructure
{
    public record KafkaTradeDTO(long Id, long Time, string Pair, double Price, double Quantity, double TotalValue) { }
}

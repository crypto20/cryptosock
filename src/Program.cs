﻿using Confluent.Kafka;
using Confluent.SchemaRegistry;
using Confluent.SchemaRegistry.Serdes;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using org.clebi.cryptosock.application;
using org.clebi.cryptosock.domain;
using org.clebi.cryptosock.exposition;
using org.clebi.cryptosock.infrastructure;
using Websocket.Client;

namespace org.clebi.cryptosock
{
    internal class TradeProducer
    {
        private static string GetEnvConfig(string name)
        {
            var env = Environment.GetEnvironmentVariable(name);
            if (env == null)
            {
                throw new ArgumentNullException(String.Format("unable to get env {0}", name));
            }
            return env;
        }

        private static void Main()
        {
            var exitEvent = new ManualResetEvent(false);

            var wsUrl = GetEnvConfig("BINANCE_WS_URL");
            var kafkaUrl = GetEnvConfig("KAFKA_BOOTSTRAP_URL");
            var registryUrl = GetEnvConfig("REGISTRY_URL");

            var host = Host.CreateDefaultBuilder()
                .ConfigureServices((_, services) =>
                    services.AddSingleton<IWebsocketClient>((_) => new WebsocketClient(new Uri(wsUrl)))
                    .AddSingleton<ISchemaRegistryClient>((_) => new CachedSchemaRegistryClient(new SchemaRegistryConfig { Url = registryUrl }))
                    .AddTransient((services) => new ProducerBuilder<long, KafkaTradeDTO>(new ProducerConfig { BootstrapServers = kafkaUrl })
                        .SetValueSerializer(
                            new JsonSerializer<KafkaTradeDTO>(
                                services.GetService<ISchemaRegistryClient>(),
                                new JsonSerializerConfig { BufferBytes = 100 }))
                        .Build())
                    .AddTransient<ITradeRepository, KafkaTradeRepository>()
                    .AddTransient<Trader>()
                    .AddTransient<IWsConsumer, BinanceWSConsumer>())
                .Build();

            var tradeConsumer = host.Services.GetRequiredService<IWsConsumer>();
            tradeConsumer.Consume();

            exitEvent.WaitOne();
        }
    }
}

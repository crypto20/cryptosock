namespace org.clebi.cryptosock.domain
{
    public record Pair(string Symbol) { }
    public record Amount(double Price, double Quantity)
    {
        public readonly double Total = Price * Quantity;
    }
    public record Transaction(long Buyer, long Seller, long Time, bool MarketMaker) { }
    public record Trade(long Id, Pair Pair, Amount Amount, Transaction Transaction) { }

    public interface ITradeRepository
    {
        void Save(Trade trade);
    }
}
